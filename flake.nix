{
  description = ''Simple, yet powerful TTY "Clipboard" tool'';

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
    poetry.url = "github:nix-community/poetry2nix";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    poetry,
    ...
  }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [poetry.overlay];
      };
      inherit (pkgs) poetry2nix;
      poetryEnv = poetry2nix.mkPoetryEnv {
        projectDir = ./.;
        preferWheels = true;
      };
    in rec {
      packages.schowek = poetry2nix.mkPoetryApplication {
        projectDir = ./.;
        python = pkgs.python310;
        preferWheels = true;
        propagatedBuildInputs = with pkgs; [tree];
      };

      packages.default = packages.schowek;

      apps.schowek = utils.lib.mkApp {
        drv = packages.schowek;
      };

      apps.default = apps.schowek;

      devShells.default = poetryEnv.env.overrideAttrs (oldAttrs: {
        buildInputs = [pkgs.python310Packages.black pkgs.python310Packages.poetry];
      });

      formatter = pkgs.alejandra;
    });
}
