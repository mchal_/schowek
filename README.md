# Schowek 

Simple, yet powerful TTY "Clipboard" tool

## 💡 Features

- Beautiful and powerful CLI using Python's [typer](https://typer.tiangolo.com/) library
- Simple and intuitive file copying/pasting directly in the terminal!
- Support for multiple named clipboards
- **zstd** compressed by default

## 💾 Installation

### 💽 From Binary

**Install Schowek using `nix profile`**
```bash
  $ nix profile install gitlab:mchal_/schowek
  $ schowek --help
```

## 📸 Screenshots

![Schowek Screenshot](https://media.discordapp.net/attachments/799373955324313613/1053364495331897364/image.png?width=572&height=454)

## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)
