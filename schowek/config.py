from getpass import getuser

from schowek.strings import info
from schowek.utils import schowek_home


def read_config():
    config = schowek_home / "current_stash"

    if not config.exists():
        default = getuser()
        with open(config, "x") as config:
            config.write(default)
            return default

    with open(config, "r") as config:
        return config.read()


def write_config(stash: str):
    config = schowek_home / "current_stash"
    with open(config, "w") as config:
        info(f"Switching current stash to {stash}")
        config.write(stash)
