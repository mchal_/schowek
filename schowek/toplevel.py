import typer

from schowek.main import stash_, paste

stash_app = typer.Typer(rich_markup_mode="rich")
stash_app.command()(stash_)

paste_app = typer.Typer(rich_markup_mode="rich")
paste_app.command()(paste)
