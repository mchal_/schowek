from getpass import getuser

import typer

from schowek.config import read_config, write_config
from schowek.utils import Stash, schowek_home


def init():
    if not schowek_home.exists():
        schowek_home.mkdir()

    return typer.Typer(no_args_is_help=True, rich_markup_mode="rich")


app = init()


@app.callback()
def callback():
    """
    📦 [bold]Simple[/bold], yet [bold]powerful[/bold] TTY "Clipboard" tool

    - Supports multiple [bold]"stashes"[/bold] the user can switch between
    - Supports stashing based on shell glob patterns
    - Stashes are [bold]zstd[/bold] compressed by default
    """


@app.command(name="stash")
def stash_(
    items: str = typer.Argument(..., help="The item(s) to stash"),
    stash: str = typer.Option(
        read_config(), help="Whether to use the default, or a custom stash"
    ),
):
    """
    [bold green]Copies[/bold green] items into the stash
    """
    Stash(stash).stash(items)


@app.command()
def paste(
    stash: str = typer.Option(
        read_config(), help="Whether to use the default, or a custom stash"
    ),
    out: str = typer.Option(".", help="The path to paste the stash contents to"),
):
    """
    [bold blue]Pastes[/bold blue] items from the stash
    """
    Stash(stash).paste(out)


@app.command()
def clear(
    stash: str = typer.Option(
        read_config(), help="Whether to use the default, or a custom stash"
    ),
):
    """
    [bold red]Clears[/bold red] the stash
    """
    Stash(stash).clear()


@app.command(name="list")
def list_(
    stash: str = typer.Option(
        read_config(), help="Whether to use the default, or a custom stash"
    ),
):
    """
    [bold magenta]Lists[/bold magenta] all entries in the stash
    """
    Stash(stash).list()


@app.command
def switch(stash: str = typer.Argument(getuser(), help="The stash name to switch to")):
    write_config(stash)
