import os
from dataclasses import dataclass
from glob import glob
from pathlib import Path

import typer
import xtarfile as tarfile

from schowek.strings import info, error

schowek_home = Path(typer.get_app_dir("schowek"))


@dataclass
class Stash:
    name: str

    def stash(self, items: str):
        info(f'Stashing all paths matching "{items}"')
        paths = glob(items)

        stash_ = schowek_home / (self.name + ".tar.zst")
        if stash_.exists():
            os.remove(stash_)

        with tarfile.open(str(stash_), "w:zstd") as archive:
            for file in paths:
                archive.add(file)

    def paste(self, out: str):
        info(f"Pasting from stash to {out}")
        out = Path(out)

        if not out.exists():
            out.mkdir(parents=True)

        stash_ = schowek_home / (self.name + ".tar.zst")

        with tarfile.open(str(stash_), "r:zstd") as archive:
            archive.extractall(out)

    def clear(self):
        info(f"Clearing stash")
        stash_ = schowek_home / (self.name + ".tar.zst")
        os.remove(stash_)

    def list(self):
        info(f"Listing stash contents")
        stash_ = schowek_home / (self.name + ".tar.zst")

        if not Path(stash_).exists():
            error("Stash is empty or doesn't exist")

        with tarfile.open(str(stash_), "r:zstd") as archive:
            archive.list(verbose=False)
