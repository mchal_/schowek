from rich import print


def info(text: str):
    print(f"[bold white]📦 {text}[/bold white]")


def error(text: str):
    print(f"[bold red]❌ {text}[/bold red]")
    exit(1)
